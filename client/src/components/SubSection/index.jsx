import React from 'react';

import FormField from '../FormField';

import './index.css';

const SubSection = ({ title, questions, onChange }) => (
  <div className="sub-section">
    <h5 className="sub-section__title">{title}</h5>
    {
      questions.map(question => (
        <FormField
          key={question.id}
          { ...question }
          onChange={onChange}
          />
      ))
    }
  </div>
);

export default SubSection;
