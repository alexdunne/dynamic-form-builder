import React from 'react';

import InputField from './InputField';
import CheckboxField from './CheckboxField';

const renderInputField = (props) => (
  <InputField {...props} />
);

const renderCheckboxField = (props) => (
  <CheckboxField {...props} />
);

const fieldMapping = {
  input: renderInputField,
  checkbox: renderCheckboxField,
};

const FormField = (props) => (
  <div>
    { fieldMapping[props.type]({...props}) }
  </div>
);

export default FormField;