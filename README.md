# Format

Section - General Information/
Subsection - The premises
Question - Number of floors

```
const sections = [{
  title: 'General Information',
  subSections: {
    title: 'The premises',
    questions: [{
      label: 'Number of floors',
      type: 'input'
      subType: 'number',
      name: 'number_of_floors',
      defaultValue: null,
    }]
  },
}];
```