import React from 'react';
import Checkbox from 'material-ui/Checkbox';

const CheckboxField = ({ id, label, name, value, onChange }) => (
  <Checkbox
    data-id={id}
    label={label}
    checked={value}
    name={name}
    onCheck={onChange}
  />
);

export default CheckboxField;