import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import FormBuilder from './components/FormBuilder';

import data from './data.json';

class App extends Component {
  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
  }

  componentWillMount() {
    this.setState({
      title: data.title,
      sections: data.sections,
    });
  }
  
  onChange(e) {
    const id = parseInt(e.target.dataset.id, 10);

    const newSections = this.state.sections.map(section => {
      const subSections = section.subSections.map(subSection => {
        const questions =  subSection.questions.map(question => {
          if (question.id === id) {
            let value = e.target.value;

            if (e.target.type === 'checkbox') {
              value = value === 'on';
            }
            question.value = value;
          }

          return question;
        });

        return {
          id: subSection.id,
          title: subSection.title,
          questions: questions,
        };
      });

      return {
        id: section.id,
        title: section.title,
        subSections: subSections,
      } 
    });

    this.setState({ sections: newSections });
  }

  render() {
    const { title, sections } = this.state;

    return (
      <MuiThemeProvider>
        <FormBuilder
          title={title}
          sections={sections}
          onChange={this.onChange} />
      </MuiThemeProvider>
    );
  }
}

export default App;
