var CACHE_NAME = 'dynamic-form-builder-cache';

self.addEventListener('install', function(e) {
  console.log('Installing service worker');

  // Cache the resources
  e.waitUntil(cacheResources());
});

self.addEventListener('fetch', function(event) {
  console.log('Service worker fetching ' + event.request.url);

  // Respond with a cached resource
  event.respondWith(getCachedResource(event.request));

  // Try to fetch the resource and update the cache if successful
  event.waitUntil(updateCachedResource(event.request));
});

function cacheResources() {
  caches.open(CACHE_NAME).then(function(cache) {
    return cache.addAll([
      '/static/js/bundle.js',
    ]);
  });
}

function getCachedResource(requestUrl) {
  return caches.open(CACHE_NAME).then(function(cache) {
    return cache.match(requestUrl).then(function(matchingResouce) {
      return matchingResouce || Promise.reject('no-match');
    });
  });
}

function updateCachedResource(requestUrl) {
  return caches.open(CACHE_NAME).then(function(cache) {
    return fetch(requestUrl).then(function(resource) {
      return cache.put(requestUrl, resource);
    });
  });
}


