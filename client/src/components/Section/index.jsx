import React from 'react';
import Subheader from 'material-ui/Subheader';

import SubSection from '../SubSection';

const Section = ({ title, subSections, onChange }) => (
  <section>
    <Subheader>{title}</Subheader>
    {
      subSections.map(subSection => (
        <SubSection key={subSection.id} {...subSection} onChange={onChange} />
      ))
    }
  </section>
);

export default Section;