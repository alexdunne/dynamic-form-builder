import React from 'react';
import ReactPaginate from 'react-paginate';

import './index.css';

const Pagination = ({ pageCount, rangeShown, marginPagesDisplayed, onChange }) => (
  <ReactPaginate
    pageCount={pageCount}
    pageRangeDisplayed={rangeShown}
    marginPagesDisplayed={marginPagesDisplayed}
    onPageChange={onChange}
    previousLabel={"Previous"}
    nextLabel={"Next"}
    containerClassName={"pagination"}
    pageClassName={"pagination__page"}
    previousClassName={"pagination__page"}
    nextClassName={"pagination__page"}
    activeClassName={"pagination__page--active"}
    pageLinkClassName={"pagination__link link link--no-outline"} 
    previousLinkClassName={"pagination__link--btn link link--no-outline"} 
    nextLinkClassName={"pagination__link--btn link link--no-outline"} 
    />
);

export default Pagination;