import React from 'react';
import TextField from 'material-ui/TextField';

const InputField = ({ id, label, name, value, onChange }) => (
  <TextField
    data-id={id}
    floatingLabelText={label}
    name={name}
    value={value}
    fullWidth={true}
    onChange={onChange}
    />
);

export default InputField;