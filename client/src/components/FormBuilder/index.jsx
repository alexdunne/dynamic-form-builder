import React, { Component } from 'react';
import Paper from 'material-ui/Paper';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';

import Pagination from '../Pagination';
import Section from '../Section';

import './index.css';

class FormBuilder extends Component {
  state = {
    pageNumber: 0,
  };

  constructor(props) {
    super(props);

    this.handlePageClick = this.handlePageClick.bind(this);
  }
  
  handlePageClick(e) {
    const { selected } = e;
    this.setState({pageNumber: selected})
  }

  render() {
    const { pageNumber } = this.state;
    const { title, sections, onChange } = this.props;
    const isFinished = (pageNumber + 1) === sections.length;

    return (
      <main className="form-builder">
        <Paper>
          <AppBar title={title} />
          <div className="form-builder__sections">
            {
              sections
                .filter((section, index) => index === pageNumber)
                .map(section => (
                  <Section
                    key={section.id}
                    title={section.title}
                    subSections={section.subSections}
                    onChange={onChange} />
                ))
            }
          </div>

          { isFinished && 
            <div className="form-builder__submit">
              <RaisedButton label="Submit" primary={true} />
            </div> 
          }

          <Pagination
            pageCount={sections.length}
            rangeShown={5}
            marginPagesDisplayed={2}
            onChange={this.handlePageClick} />
        </Paper>
      </main>
    );
  }
}

export default FormBuilder;